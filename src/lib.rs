#![cfg_attr(feature = "pedantic", warn(clippy::pedantic))]
#![warn(clippy::use_self)]
#![warn(deprecated_in_future)]
#![warn(future_incompatible)]
#![warn(unreachable_pub)]
#![warn(missing_debug_implementations)]
#![warn(rust_2018_compatibility)]
#![warn(rust_2018_idioms)]
#![warn(unused)]
// #![deny(warnings)]
#![feature(todo_macro)]

use std::fs;
use std::io;
use std::ops::Not;
use std::path::Path;

#[derive(Debug)]
pub struct ExFat {
    dev: fs::File,
}

impl ExFat {
    pub fn mkfs(dev: impl AsRef<Path>, options: impl Into<Option<Options>>) -> io::Result<Self> {
        let _options = options.into().unwrap_or_default();
        let dev = fs::OpenOptions::new().write(true).open(dev)?;
        let fs = Self { dev };
        Ok(fs)
    }

    pub fn mount(dev: impl AsRef<Path>, options: impl Into<Option<Options>>) -> io::Result<Self> {
        let options = options.into().unwrap_or_default();
        let dev = fs::OpenOptions::new()
            .read(true)
            .write(options.rdwr())
            .open(dev)?;
        let fs = Self { dev };
        Ok(fs)
    }
}

#[derive(Debug)]
pub struct Options {
    readonly: bool,
}

impl Default for Options {
    fn default() -> Self {
        Self { readonly: false }
    }
}

impl Options {
    fn rdwr(&self) -> bool {
        self.readonly.not()
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
